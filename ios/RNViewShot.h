#import <React/RCTBridgeModule.h>
#import <UIKit/UIKit.h>

@interface RNViewShot : NSObject <RCTBridgeModule>

- (BOOL) cropImage:(UIImage*)sourceImage
          withRect:(CGRect)rect
            toView:(UIImageView*)imageView;
@end
  
